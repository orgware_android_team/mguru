package com.orgware.mguru.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.orgware.mguru.R;
import com.orgware.mguru.activity.MainActivity;

/**
 * Created by Orgware on 8/24/2015.
 */
public class UnitSelectionPage extends BaseFragment implements View.OnClickListener {
    @Override
    void setTitle() {
        View v = inflater.inflate(R.layout.item_action_bar_image_button, null);
        ((MainActivity) act).setTitle("Maths", v);
        ImageButton back = (ImageButton) v.findViewById(R.id.action_image_button);
        back.setBackgroundResource(R.drawable.ic_back);
        back.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_unit_selection, container, false);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_image_button:
                act.getSupportFragmentManager().popBackStack();
                break;

        }
    }
}
