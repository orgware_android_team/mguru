package com.orgware.mguru.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.orgware.mguru.R;
import com.orgware.mguru.activity.fragment.CourseSelectionPage;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;


public class MainActivity extends BaseActivity {

    MenuDrawer slider;
    CourseSelectionPage courseSelectionPage;
    LinearLayout frameLayout_image, mLay_maths, mLay_english, mLay_science;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isScreenXlarge) {
            slider = MenuDrawer.attach(activity, MenuDrawer.Type.OVERLAY,
                    Position.RIGHT);
        } else {
            slider = MenuDrawer.attach(activity, MenuDrawer.Type.OVERLAY,
                    Position.RIGHT);
            slider.setOffsetMenuEnabled(false);
        }
        slider.setContentView(R.layout.activity_main);
        slider.setMenuView(R.layout.fragment_slider_menu);
        slider.setMenuSize(menuWidth);
        slider.setOnDrawerStateChangeListener(listener);
        frameLayout_image = (LinearLayout) findViewById(R.id.action_bar_actions);

        if (courseSelectionPage == null) {
            courseSelectionPage = new CourseSelectionPage();
            setNewFragment(courseSelectionPage, "", false);
        }


    }

    MenuDrawer.OnDrawerStateChangeListener listener = new MenuDrawer.OnDrawerStateChangeListener() {

        @Override
        public void onDrawerStateChange(int oldState, int newState) {
            findViewById(R.id.fragment_content).clearFocus();
        }

        @Override
        public void onDrawerSlide(float openRatio, int offsetPixels) {
        }
    };

    @Override
    public void onBackPressed() {

        if (slider.isMenuVisible() && !isScreenXlarge) {
            slider.closeMenu();
            return;
        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }
        if (getSupportFragmentManager().findFragmentById(R.id.fragment_content)
                .getClass() == CourseSelectionPage.class)
            super.onBackPressed();
        else {
            if (courseSelectionPage == null)
                courseSelectionPage = new CourseSelectionPage();
            setNewFragment(courseSelectionPage, "DashBoard", false);
        }
    }

    public void setTitle(String title, View... views) {
        setText(title, R.id.page_title);
        frameLayout_image.removeAllViews();
        if (views == null)
            return;
        for (View single : views) {
            frameLayout_image.addView(single);
            // if (views.length > 1) {
            // ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams)
            // single
            // .getLayoutParams();
            // p.setMargins(
            // 0,
            // 0,
            // getResources().getDimensionPixelSize(
            // R.dimen.item_left_margin), 0);
            // single.requestLayout();
            // }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void openSlider(View v) {
        slider.toggleMenu();
    }


    private void clearBackStack() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            return;

        getSupportFragmentManager().popBackStack(null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Fragment cFragment = getSupportFragmentManager().findFragmentById(
                R.id.fragment_content);
        if (cFragment != null)
            getSupportFragmentManager().beginTransaction().remove(cFragment)
                    .commit();
    }

    public void setNewFragment(Fragment fragment, String title,
                               boolean addbackstack) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_content, fragment);
        if (addbackstack)
            transaction.addToBackStack(title);
        transaction.commit();

    }

    public void setNewFragmentVideo(Fragment fragment, String title,
                                    boolean addbackstack, String key, String path) {

        Bundle b = new Bundle();
        b.putString(key, path);
        fragment.setArguments(b);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_content, fragment);
        if (addbackstack)
            transaction.addToBackStack(title);
        transaction.commit();

    }


}
