package com.orgware.mguru.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.orgware.mguru.R;

/**
 * Created by WindowsMobile on 8/24/2015.
 */
public class SignUpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

    }

    public void onSignUp(View v) {
        finish();
    }
}
