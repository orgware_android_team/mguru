package com.orgware.mguru.activity.adapter;

import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orgware.mguru.activity.utils.AppInterface;


/* Parent class for listview and gridview adapter */
public class BaseAdapter<T> extends ArrayAdapter<T> implements AppInterface {

    LayoutInflater inflater;
    Typeface typefaceMuliRegular;
    int resource;
    List<T> cList;
    JSONArray jsonArray;

    enum HeaderFooter {
        HEADER, FOOTER, NONE;
    }

    /* Constructor initialize the objects, inflater, font typeface */
    public BaseAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        cList = objects;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typefaceMuliRegular = Typeface.createFromAsset(context.getAssets(),
                "IDEALIST SANS LIGHT_0.TTF");
        this.resource = resource;
    }

    public BaseAdapter(Context context, int resource, JSONArray objects) {
        super(context, resource);
        jsonArray = objects;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typefaceMuliRegular = Typeface.createFromAsset(context.getAssets(),
                "font/HERO_0.OTF");
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return cList != null ? cList.size() : jsonArray.length();
    }

    /* Method returns the color for parameter id */
    int getColor(int id) {
        return getContext().getResources().getColor(id);
    }

    @Override
    public View getView(int position, View cView, ViewGroup parent) {
        changeTypeface((ViewGroup) cView);
        return cView;
    }

    void setHeaderFooter(View v, int pos) {

        // if (pos == 0) {
        // v.findViewById(R.id.header).setVisibility(View.VISIBLE);
        // v.findViewById(R.id.footer).setVisibility(View.GONE);
        // } else if (pos == getCount() - 1) {
        // v.findViewById(R.id.header).setVisibility(View.GONE);
        // v.findViewById(R.id.footer).setVisibility(View.VISIBLE);
        // } else {
        // v.findViewById(R.id.header).setVisibility(View.GONE);
        // v.findViewById(R.id.footer).setVisibility(View.GONE);
        // }

    }

    /* Method changes the default font typeface to muli */
    private void changeTypeface(ViewGroup vGroup) {
        for (int i = 0; i < vGroup.getChildCount(); i++) {
            View v = vGroup.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(typefaceMuliRegular);
            } else if (v instanceof RadioButton) {
                ((RadioButton) v).setTypeface(typefaceMuliRegular);
            } else if (v instanceof DrawerLayout || v instanceof FrameLayout
                    || v instanceof LinearLayout || v instanceof RelativeLayout
                    || v instanceof RadioGroup || v instanceof ListView) {
                changeTypeface((ViewGroup) v);
            }
        }
    }

    void setVisible(View view) {
        view.setVisibility(View.VISIBLE);
    }

    void setInvisible(View view) {
        view.setVisibility(View.INVISIBLE);
    }

    void setGone(View view) {
        view.setVisibility(View.GONE);
    }

    // String checkURL(String url) {
    // return MethodUtil.checkURL(getContext(), url);
    // }
}
