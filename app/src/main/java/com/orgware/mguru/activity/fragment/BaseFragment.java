package com.orgware.mguru.activity.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orgware.mguru.activity.utils.AppInterface;


public abstract class BaseFragment extends Fragment implements AppInterface {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    LayoutInflater inflater;
    FragmentActivity act;
    private Typeface typeface;
    protected View mParentView;

    abstract void setTitle();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act = (FragmentActivity) getActivity();
        preferences = act.getSharedPreferences(SHARED_DATA, 0);
        editor = preferences.edit();
        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(act.getAssets(), "IDEALIST SANS LIGHT_0.TTF");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mParentView = view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mParentView != null)
            changeTypeface((ViewGroup) mParentView);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();

    }

    protected void changeTypeface(ViewGroup vGroup) {
        for (int i = 0; i < vGroup.getChildCount(); i++) {
            View v = vGroup.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(typeface);
            } else if (v instanceof RadioButton) {
                ((RadioButton) v).setTypeface(typeface);
            } else if (v instanceof DrawerLayout || v instanceof FrameLayout
                    || v instanceof LinearLayout || v instanceof RelativeLayout
                    || v instanceof RadioGroup || v instanceof ListView) {
                changeTypeface((ViewGroup) v);
            }
        }
    }

    Fragment setFArgs(Fragment f, String... param) {
        Bundle args = new Bundle();
        for (int i = 0; i < param.length; i += 2) {
            args.putString(param[i], param[i + 1]);
        }
        f.setArguments(args);
        return f;
    }

    // Fragment setPageFragment(Fragment fragment, String title) {
    // Bundle b = new Bundle();
    // b.putString("title", title);
    // fragment.setArguments(b);
    // return fragment;
    // }

}
