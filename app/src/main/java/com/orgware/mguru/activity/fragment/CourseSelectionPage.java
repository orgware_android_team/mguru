package com.orgware.mguru.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.orgware.mguru.R;
import com.orgware.mguru.activity.MainActivity;

/**
 * Created by Orgware on 8/24/2015.
 */
public class CourseSelectionPage extends BaseFragment implements View.OnClickListener {

    LinearLayout mLay_maths, mLay_english, mLay_science;

    @Override
    void setTitle() {
        View v = inflater.inflate(R.layout.item_action_bar_image_button, null);
        ((MainActivity) act).setTitle("Hi Andrew", v);
        ImageButton imageButton = (ImageButton) v.findViewById(R.id.action_image_button);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_course_selection, container, false);
        mLay_maths = (LinearLayout) v.findViewById(R.id.lay_maths);
        mLay_english = (LinearLayout) v.findViewById(R.id.lay_english);
        mLay_science = (LinearLayout) v.findViewById(R.id.lay_science);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLay_maths.setOnClickListener(this);
        mLay_english.setOnClickListener(this);
        mLay_science.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_maths:
                ((MainActivity) act).setNewFragment(new UnitSelectionPage(), "", true);
                break;
            case R.id.lay_english:
                ((MainActivity) act).setNewFragment(new UnitSelectionPage(), "", true);
                break;
            case R.id.lay_science:
                ((MainActivity) act).setNewFragment(new UnitSelectionPage(), "", true);
                break;
        }
    }
}
