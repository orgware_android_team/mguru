package com.orgware.mguru.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.orgware.mguru.R;

/**
 * Created by Orgware on 8/24/2015.
 */
public class LoginActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button button=new Button(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onSignUp(View v) {
        Intent go = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(go);
    }

    public void onSignIn(View v) {
        Intent go = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(go);
    }
}
