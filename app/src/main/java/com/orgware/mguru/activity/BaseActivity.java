package com.orgware.mguru.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.orgware.mguru.activity.utils.AppInterface;


public class BaseActivity extends FragmentActivity implements AppInterface {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    LayoutInflater mInflater;
    FragmentActivity activity;
    boolean isScreenXlarge;
    int heightPixel, widthPixel, menuWidth;
    private Typeface typefaceMuliRegular;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        activity = this;
        preferences = getSharedPreferences(SHARED_DATA, MODE_PRIVATE);
        editor = preferences.edit();
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typefaceMuliRegular = Typeface.createFromAsset(getAssets(),
                "IDEALIST SANS LIGHT_0.TTF");
        setOrientation();
        setMenuSize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        changeTypeface();
    }

    public void changeTypeface() {
        changeTypeface((ViewGroup) findViewById(android.R.id.content));
        // if (actionBar.isShowing())
        // changeTypeface((ViewGroup) findViewById(R.id.action_bar));
    }

    /* Method changes the text default font to muli custom font */
    private void changeTypeface(ViewGroup vGroup) {
        for (int i = 0; i < vGroup.getChildCount(); i++) {
            View v = vGroup.getChildAt(i);
            if (v instanceof ImageView || v instanceof ImageButton
                    || v instanceof ListView)
                continue;
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(typefaceMuliRegular);
            } else if (v instanceof RadioButton) {
                ((RadioButton) v).setTypeface(typefaceMuliRegular);
            } else if (v instanceof DrawerLayout || v instanceof FrameLayout
                    || v instanceof LinearLayout || v instanceof RelativeLayout
                    || v instanceof RadioGroup || v instanceof ViewGroup) {
                changeTypeface((ViewGroup) v);
            }
        }
    }

    private void setMenuSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        heightPixel = displayMetrics.heightPixels;
        widthPixel = displayMetrics.widthPixels;
        menuWidth = heightPixel < widthPixel ? heightPixel : widthPixel;
        menuWidth = (int) (menuWidth > 600 ? 900 : menuWidth * 0.8);
    }

    private void setOrientation() {

        int screen = getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK;
        if (screen == Configuration.SCREENLAYOUT_SIZE_SMALL
                || screen == Configuration.SCREENLAYOUT_SIZE_NORMAL
                || screen == Configuration.SCREENLAYOUT_SIZE_LARGE)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else if (screen == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            isScreenXlarge = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    void setText(String input, int id) {
        ((TextView) findViewById(id)).setText(input);
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        super.onActivityResult(arg0, arg1, arg2);
    }

}
