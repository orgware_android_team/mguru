package com.orgware.mguru.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.orgware.mguru.R;

import java.io.File;

/**
 * Created by Orgware on 8/24/2015.
 */
public class SplashScreen extends BaseActivity {

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_splash);
        handler.postDelayed(splashThread, 3000);
    }

    Runnable splashThread = new Runnable() {

        @Override
        public void run() {
            startActivity(new Intent(SplashScreen.this,
                    LoginActivity.class));
            finish();
        }
    };

}


